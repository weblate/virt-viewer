# Virt Viewer package strings.
# Copyright (C) 2019 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Asier Iturralde Sarasola <asier.iturralde@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: virt-viewer 9.0\n"
"Report-Msgid-Bugs-To: https://virt-manager.org/bugs/\n"
"POT-Creation-Date: 2019-12-16 15:59+0000\n"
"PO-Revision-Date: 2015-02-20 08:09+0000\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Basque (http://www.transifex.com/projects/p/virt-viewer/"
"language/eu/)\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

msgid " "
msgstr " "

#, c-format
msgid "%s%s%s - %s"
msgstr "%s%s%s - %s"

msgid "Disconnect"
msgstr "Deskonektatu"

msgid "Display debugging information"
msgstr "Bistaratu arazketa informazioa"

msgid "Display version information"
msgstr "Bistaratu bertsio informazioa"

msgid "Leave fullscreen"
msgstr "Irten pantaila osotik"

msgid "Password:"
msgstr "Pasahitza:"

msgid "USB device selection"
msgstr "USB gailuaren hautapena"

msgid "Username:"
msgstr "Erabiltzaile-izena:"

msgid "_File"
msgstr "_Fitxategia"

msgid "_Help"
msgstr "_Laguntza"

msgid "_Send key"
msgstr "_Bidali tekla"

msgid "_View"
msgstr "_Ikusi"

msgid "_Zoom"
msgstr "_Zooma"

msgid "label"
msgstr "etiketa"
