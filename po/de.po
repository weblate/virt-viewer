# Virt Viewer package strings.
# Copyright (C) 2019 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Automatically generated, 2010
# Fabian Affolter <fab@fedoraproject.org>, 2012, 2014
# hpeters <hpeters@redhat.com>, 2010, 2012
# noxin <transifex.com@davidmainzer.com>, 2013
# Rainer Gromansperg <rgromans@redhat.com>, 2013
# Roman Spirgi <bigant@fedoraproject.org>, 2012, 2015
# jasna <jdimanos@redhat.com>, 2016
# Florian H. <postfuerflo@gmail.com>, 2017
# Daniel Berrange <dan-zanata@berrange.com>, 2019
# Thomas Eichhorn <tomislav@posteo.de>, 2019
msgid ""
msgstr ""
"Project-Id-Version: virt-viewer 9.0\n"
"Report-Msgid-Bugs-To: https://virt-manager.org/bugs/\n"
"POT-Creation-Date: 2019-12-16 15:59+0000\n"
"PO-Revision-Date: 2019-03-25 08:53+0000\n"
"Last-Translator: Thomas Eichhorn <tomislav@posteo.de>\n"
"Language-Team: German (http://www.transifex.com/projects/p/virt-viewer/"
"language/de/)\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

msgid ""
"\n"
"Error: can't handle multiple URIs\n"
"\n"
msgstr ""
"\n"
"Fehler: Multiple URIs können nicht verarbeitet werden\n"
"\n"

msgid " "
msgstr " "

#, c-format
msgid "%s\n"
msgstr "%s\n"

#, c-format
msgid "%s version %s"
msgstr "%s Version %s"

#, c-format
msgid "%s%s%s - %s"
msgstr "%s%s%s - %s"

#, c-format
msgid "(Press %s to release pointer)"
msgstr "(Drücken Sie %s, um den Mauszeiger freizugeben)"

msgid "<never|on-disconnect>"
msgstr "<never|on-disconnect>"

msgid "A remote desktop client built with GTK-VNC, SPICE-GTK and libvirt"
msgstr ""
"Ein Remote-Desktop-Client erstellt unter Verwendung von GTK-VNC, SPICE-GTK "
"und libvirt"

msgid "About Virt-Viewer"
msgstr "Über Virt-Viewer"

msgid "Access remote desktops"
msgstr "Zugriff auf Remote-Desktops"

#, c-format
msgid "At least %s version %s is required to setup this connection"
msgstr ""
"Es wird mindestens %s in der Version %s benötigt, um eine Verbindung "
"aufzubauen"

#, c-format
msgid ""
"At least %s version %s is required to setup this connection, see %s for "
"details"
msgstr ""
"Es wird mindestens %s in der Version %s benötigt, um eine Verbindung "
"aufzubauen, siehe %s für Details"

msgid "Attach to the local display using libvirt"
msgstr "Mit lokaler Anzeige verbinden mittels libvirt"

msgid "Authentication failed."
msgstr "Authentifizierung fehlgeschlagen."

#, c-format
msgid ""
"Authentication is required for the %s connection to:\n"
"\n"
"<b>%s</b>\n"
"\n"
msgstr ""
"Authentifikation ist erforderlich für die %s Verbindung mit:\n"
"\n"
"<b>%s</b>\n"
"\n"

#, c-format
msgid "Authentication is required for the %s connection:\n"
msgstr "Authentifikation ist für die %s Verbindung erforderlich:\n"

msgid "Authentication required"
msgstr "Authentifikation erforderlich"

msgid "Authentication was cancelled"
msgstr "Authentifizierung abgebrochen"

msgid "Available virtual machines"
msgstr "Verfügbare virtuelle Maschinen"

msgid "C_onnect"
msgstr "V_erbinden"

msgid "Cannot determine the connection type from URI"
msgstr "Verbindungstyp konnte nicht von URI ermittelt werden"

#, c-format
msgid "Cannot determine the graphic type for the guest %s"
msgstr "Grafiktyp für den Gast %s konnte nicht ermittelt werden "

#, c-format
msgid "Cannot determine the host for the guest %s"
msgstr "Host für den Gast %s konnte nicht ermittelt werden"

msgid "Cannot get guest state"
msgstr "Gast-Status konnte nicht abgerufen werden"

msgid "Change CD"
msgstr "CD wechseln"

msgid "Checking guest domain status"
msgstr "Prüfen des Gast-Domain-Status"

msgid "Choose a virtual machine"
msgstr "Wählen Sie eine virtuelle Maschine"

msgid "Close"
msgstr "Schließen"

msgid "Connect to channel unsupported."
msgstr "Verbindung zu dem Kanal wird nicht unterstützt."

msgid "Connect to hypervisor"
msgstr "Verbinden mit Hypervisor"

msgid "Connect to ssh failed."
msgstr "Verbindung über SSH ist fehlgeschlagen."

msgid "Connected to graphic server"
msgstr "Verbunden mit Grafikserver"

msgid "Connecting to graphic server"
msgstr "Verbinden mit Grafikserver"

msgid "Connection details"
msgstr "Verbindungsdetails"

msgid "Console support is compiled out!"
msgstr "Konsolenunterstützung wurde nicht ins Programm kompiliert!"

msgid ""
"Copyright (C) 2007-2012 Daniel P. Berrange\n"
"Copyright (C) 2007-2014 Red Hat, Inc."
msgstr ""
"Copyright (C) 2007-2012 Daniel P. Berrange\n"
"Copyright (C) 2007-2014 Red Hat, Inc."

msgid "Couldn't open oVirt session: "
msgstr "Konnte oVirt-Sitzung nicht öffnen: "

#, c-format
msgid "Creating unix socket failed: %s"
msgstr "Unix-Socket konnte nicht erstellt werden: %s"

msgid "Ctrl+Alt"
msgstr "Strg+Alt"

msgid "Ctrl+Alt+F11"
msgstr "Strg+Alt+F11"

msgid "Ctrl+Alt+F12"
msgstr "Strg+Alt+F12"

msgid "Ctrl+Alt+F1_0"
msgstr "Strg+Alt+F1_0"

msgid "Ctrl+Alt+F_1"
msgstr "Strg+Alt+F_1"

msgid "Ctrl+Alt+F_2"
msgstr "Strg+Alt+F_2"

msgid "Ctrl+Alt+F_3"
msgstr "Strg+Alt+F_3"

msgid "Ctrl+Alt+F_4"
msgstr "Strg+Alt+F_4"

msgid "Ctrl+Alt+F_5"
msgstr "Strg+Alt+F_5"

msgid "Ctrl+Alt+F_6"
msgstr "Strg+Alt+F_6"

msgid "Ctrl+Alt+F_7"
msgstr "Strg+Alt+F_7"

msgid "Ctrl+Alt+F_8"
msgstr "Strg+Alt+F_8"

msgid "Ctrl+Alt+F_9"
msgstr "Strg+Alt+F_9"

msgid "Ctrl+Alt+_Backspace"
msgstr "Strg+Alt+_Rücktaste"

msgid "Ctrl+Alt+_Del"
msgstr "Strg+Alt+_Entf"

msgid "Customise hotkeys"
msgstr "eigene angepasste Hotkeys"

msgid "Direct connection with no automatic tunnels"
msgstr "Direkte Verbindung ohne automatische Tunnel"

msgid "Disconnect"
msgstr "Verbindung trennen"

msgid "Display can only be attached through libvirt with --attach"
msgstr "Anzeige kann nur durch libvirt mit --attach zugeordnet werden"

msgid "Display debugging information"
msgstr "Debugging-Informationen anzeigen"

msgid "Display verbose information"
msgstr "Ausführliche Informationen anzeigen"

msgid "Display version information"
msgstr "Versionsinformationen anzeigen"

msgid "Do not ask me again"
msgstr "Nicht noch einmal nachfragen"

msgid "Do you want to close the session?"
msgstr "Möchten Sie die Sitzung beenden?"

msgid "Enable kiosk mode"
msgstr "Kiosk Modus ermöglichen"

msgid "Failed to change CD"
msgstr "Fehler beim Wechseln der CD"

msgid "Failed to connect: "
msgstr "Verbindung fehlgeschlagen:"

msgid "Failed to fetch CD names"
msgstr "CD-Namen konnten nicht abgerufen werden"

msgid "Failed to initiate connection"
msgstr "Initiieren der Verbindung fehlgeschlagen"

msgid "Failed to read stdin: "
msgstr "stdin könnte nicht gelesen werden:"

msgid "File Transfers"
msgstr "Dateitransfer"

msgid "Finding guest domain"
msgstr "Suchen der Gast-Domain"

msgid "Folder sharing"
msgstr "Ordnerfreigabe"

msgid "For example, spice://foo.example.org:5900"
msgstr "Zum Beispiel spice://foo.example.org:5900"

msgid "GUID:"
msgstr "GUID:"

#, c-format
msgid "Guest '%s' is not reachable"
msgstr "Gast »%s« ist nicht erreichbar"

msgid "Guest Details"
msgstr "Gast Details"

msgid "Guest domain has shutdown"
msgstr "Gast-Domain wurde beendet"

#, c-format
msgid "Invalid file %s: "
msgstr "Ungültige Datei %s: "

#, c-format
msgid "Invalid kiosk-quit argument: %s"
msgstr "Ungültiger kiosk-quit Parameter: %s"

msgid "Invalid password"
msgstr "Ungültiges Passwort"

msgid "Leave fullscreen"
msgstr "Vollbildmodus verlassen"

msgid "Loading..."
msgstr "Lade..."

msgid "Name"
msgstr "Name"

msgid "Name:"
msgstr "Name:"

msgid "No connection was chosen"
msgstr "Es wurde keine Verbindung gewählt"

msgid "No virtual machine was chosen"
msgstr "Es wurde keine virtuelle Maschine ausgewählt"

msgid "Open in full screen mode (adjusts guest resolution to fit the client)"
msgstr ""
"Im Vollbildmodus öffnen (Die Auflösung des Gast-Systems entsprechend "
"angepasst an)."

msgid "Password:"
msgstr "Passwort:"

msgid "Preferences"
msgstr "Präferenzen"

msgid "Quit on given condition in kiosk mode"
msgstr "Beenden bei bestimmter Bedingung im Kiosk-Modus"

msgid "Read-only"
msgstr "Schreibgeschützt"

msgid "Recent connections"
msgstr "Neueste Verbindungen"

msgid "Reconnect to domain upon restart"
msgstr "Nach Neustart erneut mit Domain verbinden"

msgid "Release cursor"
msgstr "Cursor freigeben"

msgid "Remote Viewer"
msgstr "Remote-Viewer"

msgid "Remote viewer client"
msgstr "Remote-Viewer Client"

msgid "Remotely access virtual machines"
msgstr "Greifen Sie auf entfernte Virtuelle Maschinen zu"

#, c-format
msgid "Run '%s --help' to see a full list of available command line options\n"
msgstr ""
"Führen Sie '%s --help' aus, zum Anzeigen aller verfügbaren Kommandozeilen-"
"Optionen\n"

msgid "Select ISO"
msgstr "ISO-Abbild auswählen"

msgid "Select USB devices for redirection"
msgstr "USB-Gerät zur Weiterleitung wählen"

msgid "Selected"
msgstr "Ausgewählt"

msgid "Send key combination"
msgstr "Tastenkombination senden"

msgid "Set window title"
msgstr "Titel des Window setzen"

msgid "Share folder"
msgstr "Ordner freigeben"

msgid "Show password"
msgstr "Passwort anzeigen"

msgid "Smartcard insertion"
msgstr "Smartcard-Eingabe"

msgid "Smartcard removal"
msgstr "Smartcard-Entfernung"

msgid "Spice"
msgstr "Spice"

msgid "The Fedora Translation Team"
msgstr "Das Fedora Übersetzungs-Team"

msgid ""
"This program is free software; you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation; either version 2 of the License, or\n"
"(at your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License\n"
"along with this program; if not, write to the Free Software\n"
"Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n"
msgstr ""
"This program is free software; you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation; either version 2 of the License, or\n"
"(at your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License\n"
"along with this program; if not, write to the Free Software\n"
"Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n"

msgid "Transferring 1 file..."
msgstr "Eine Datei wird übertragen..."

msgid "USB device selection"
msgstr "USB-Geräteauswahl"

#, c-format
msgid "USB redirection error: %s"
msgstr "USB-Weiterleitungsfehler: %s"

#, c-format
msgid "Unable to authenticate with remote desktop server at %s: %s\n"
msgstr "Authentifikation beim Remote-Desktop-Server an %s: %s fehlgeschlagen\n"

#, c-format
msgid "Unable to authenticate with remote desktop server: %s"
msgstr "Authentifikation beim Remote-Desktop-Server fehlgeschlagen: %s"

#, c-format
msgid "Unable to connect to libvirt with URI: %s."
msgstr "Verbindung zu libvirt mit URI: %s nicht möglich."

#, c-format
msgid "Unable to connect to the graphic server %s"
msgstr "Verbindung zum Grafik-Server %s konnte nicht hergestellt werden"

#, c-format
msgid "Unable to connect: %s"
msgstr "Verbindung konnte nicht hergestellt werden: %s"

msgid "Unknown"
msgstr "Unbekannt"

msgid "Unspecified error"
msgstr "Unspezifizierter Fehler"

#, c-format
msgid "Unsupported graphic type '%s'"
msgstr "Nicht unterstützter Grafiktyp »%s«"

msgid "Username:"
msgstr "Benutzername:"

msgid "VNC does not provide GUID"
msgstr "VNC bietet keine GUID"

msgid "Virt Viewer"
msgstr "Virt-Viewer"

#, c-format
msgid "Virtual machine %s is not running"
msgstr "Virtuelle Maschine %s läuft nicht"

msgid "Virtual machine graphical console"
msgstr "Grafische Konsole der virtuellen Maschine"

msgid "Wait for domain to start"
msgstr "Warten auf Start der Domain"

#, c-format
msgid "Waiting for display %d..."
msgstr "Warten auf Anzeige %d ..."

msgid "Waiting for guest domain to be created"
msgstr "Warten auf Erstellung der Gast-Domain"

msgid "Waiting for guest domain to re-start"
msgstr "Warten auf Neustart der Domain"

msgid "Waiting for guest domain to start"
msgstr "Warten auf Start der Gast-Domain"

msgid "Waiting for libvirt to start"
msgstr "Auf den Start von libvirt wird gewartet"

msgid "Zoom _In"
msgstr "Ver_größern"

msgid "Zoom _Out"
msgstr "Ver_kleinern"

#, c-format
msgid "Zoom level must be within %d-%d\n"
msgstr "Zoom-Level muss zwischen %d-%d sein\n"

msgid "Zoom level of window, in percentage"
msgstr "Zoomlevel des Fensters in Prozent"

msgid "[none]"
msgstr "[keine]"

msgid "_About"
msgstr "Ü_ber"

msgid "_Cancel"
msgstr "_Abbrechen"

msgid "_Change CD"
msgstr "_CD wechseln"

msgid "_Close"
msgstr "_Schließen"

msgid "_Displays"
msgstr "_Anzeigen"

msgid "_File"
msgstr "_Datei"

msgid "_Full screen"
msgstr "_Vollbild"

msgid "_Guest Details"
msgstr "_Gast Details"

msgid "_Help"
msgstr "_Hilfe"

msgid "_Normal Size"
msgstr "_Standardgröße"

msgid "_OK"
msgstr "_OK"

msgid "_Preferences"
msgstr "_Einstellungen"

msgid "_Quit"
msgstr "_Quit"

msgid "_Save"
msgstr "_Speichern"

msgid "_Screenshot"
msgstr "_Bildschirmfoto"

msgid "_Send key"
msgstr "_Taste senden"

msgid "_USB device selection"
msgstr "_USB-Geräteauswahl"

msgid "_View"
msgstr "_Ansicht"

msgid "_Zoom"
msgstr "_Zoom"

msgid "failed to parse ovirt uri"
msgstr "Parsen von ovirt uri fehlgeschlagen"

msgid "label"
msgstr "Label"

#, c-format
msgid "oVirt VM %s has no display"
msgstr "oVirt VM %s hat keine Anzeige"

#, c-format
msgid "oVirt VM %s has no host information"
msgstr "oVirt VM %s hat keine Host-Informationen"

#, c-format
msgid "oVirt VM %s is not running"
msgstr "oVirt VM %s läuft nicht"

msgid "only SSH or unix socket connection supported."
msgstr "Es werden nur SSH und Unix-Sockets unterstützt."

msgid "virt-manager.org"
msgstr "virt-manager.org"

msgid "virt-viewer"
msgstr "virt-viewer"
